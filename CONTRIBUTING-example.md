Assuming you have read README.md and are already comfortable about
1. What the project does
2. Why we are building the project

..in Contributing.md we would talk about:

[[_TOC_]]  

# How to run it
Check some release branches deployed on the cloud by visiting
* https://link1-deployed-by-cicid/
* https://link2-deployed-by-cicid/

To get everything running on your local developer machine, fork the `develop` branch. Watch [this 45 min video](https://gitlab.com/smarter-codes/guidelines/software-engineering/coding/about-coding/-/issues/5#note_533170443) to understand how branching is working in this project

## Dependencies
Once you have the code on your machine, we now ask you to instlal dependencies. Than insisting you to install the depencies manually on your machine, we have made this script for you. Just fire the command below
```
bash command here
```
And all dependencies would be installed in a safe sandbox environment in your developer machine. Without touching any existing dependency on your machine

There are still some dependencies that you machine must have so that above command can run! Ensure that you have the following before you fire above command
* Dependency A. [Link to installation steps]()
* Dependency B. [Link to installation steps]()

## Special hardware required?
A very few applications we build, they require special hardware (minimum RAM, a piece of hardware attached). This software can run in a docker container. And does not takes more than 100MB of RAM or Hardisk. So no need to bother about any special hardware here

## How to test

## Other modes to run

# Technology Stack

# Guidelines

## [Requirements](https://gitlab.com/smarter-codes/guidelines/software-engineering/requirements)

- [Writing Issues: WHAT, WHY and HOW](https://gitlab.com/smarter-codes/guidelines/software-engineering/requirements/documenting-requirements/-/issues/1)

## [Architecture and Design](https://gitlab.com/smarter-codes/guidelines/software-engineering/architecture-design)
## [Coding](https://gitlab.com/smarter-codes/guidelines/software-engineering/coding)

- [Git Branching Model](https://gitlab.com/smarter-codes/guidelines/software-engineering/coding/about-coding/-/issues/5)
- [Update README.md file for (almost) every pull request](https://gitlab.com/smarter-codes/guidelines/software-engineering/coding/about-coding/-/issues/7)
- [Test Driven Development](https://gitlab.com/smarter-codes/guidelines/software-engineering/coding/general-coding/test-driven-development/-/issues/1)
- [Writing code for humans](https://gitlab.com/smarter-codes/guidelines/software-engineering/coding/about-coding/-/issues/2)
- [Multi word component names](https://gitlab.com/smarter-codes/guidelines/software-engineering/coding/frontend/frameworks/vuejs/guidelines/-/issues/1)
- [Using Vuex Store - When and How](https://gitlab.com/smarter-codes/guidelines/software-engineering/coding/frontend/frameworks/vuejs/guidelines/-/issues/2)

## [Deploy](https://gitlab.com/smarter-codes/guidelines/software-engineering/deploy)

- [A deployment for every branch](https://gitlab.com/smarter-codes/guidelines/software-engineering/deploy/cicd/-/issues/1)

## [Monitor](https://gitlab.com/smarter-codes/guidelines/software-engineering/monitor)
